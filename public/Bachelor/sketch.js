let museum;

function preload(){
  museum = createImg("museum.webp");
}

function setup() {
  createCanvas(1200, 800);
}

function draw() {
  museum.size(1200,800);
  museum.position(0,0);
}

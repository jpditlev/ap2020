let words;
let myfont;


function preload(){
  words = loadJSON("words.json");
  myfont = loadFont("myfont.ttf");
}



function setup(){
  createCanvas(windowWidth,windowHeight);
}



function draw(){
textFont(myfont); //calling the loaded font to be used
noLoop(); //Draw stops looping so only 1 poem is generated.

textSize(25);
fill(0);
text(random(words.nouns) + " " + random(words.verbs) + " " + random(words.nouns),100,170);
text(random(words.time) + " " + random(words.adjs) + " " + random(words.nouns2)+ " " + random(words.verbs3),100,230);
text(random(words.adjs) + " " + random(words.nouns2) + " " + random(words.verbs2),100,290);
//the three sentences that makes the haiku poem. The words are called from the json file
//and "+" is used too include mulitple commands to fetch words form json, but also
//to add spaces between the words. Otherwise all the words would just stand
//in extension of one another.
}

function mousePressed() {
  clear();
  loop();
//mousePressed is here used to clear the screen of the first poem and to make the program loop again.
//then noLoop is run in draw, and the program again stops looping, creating only 1 poem.
}

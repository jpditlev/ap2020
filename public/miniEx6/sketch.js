let toilet = []
let min_toilet = 5


function setup(){
  createCanvas(windowWidth,windowHeight);

  for (let i=0; i<=min_toilet; i++) {
    toilet[i] = new Toiletpaper(); //create/construct a new object instance
  }


}
function draw(){
  background(255);
  showToiletpaper();
  checkToilet();
  checkToiletNum();
}


function mousePressed(){
  for (let i = 0; i <toilet.length; i++) {
    toilet[i].clicked(mouseX, mouseY); //here I check the position of where mouse is clicked in relaiton to an object from the array
  }

}




function showToiletpaper(){
  for (let i = 0; i <toilet.length; i++) {
    toilet[i].move();
    toilet[i].show();
  }
}

function checkToilet() {

  for (let i = 0; i <toilet.length; i++) {
    if (toilet[i].ypos > windowHeight) {

      toilet.splice(i, 1); //if toiletpaper falls from the screen, it is deleted from the array
    }
  }
}

function checkToiletNum() {

  if (toilet.length < min_toilet) {
    toilet.push(new Toiletpaper()); //if the number of toiletpaper rolls is less than the minimum, another is added
  }
}



class Toiletpaper {
  constructor(xpos, ypos, speed, size) {

    this.xpos = random(100,windowWidth-300)
    this.ypos = random(20)
    this.speed = random(2,5)
    this.size = random(25)

  }

  clicked(xx,yy){
    let d = dist(xx, yy, this.xpos, this.ypos);
    if (d < this.size){
      print("SUCCES");
    }
  }

  show() {
    push();
    translate(this.xpos, this.ypos); //I use translate because I want to move the entire toiletpaper roll
    fill(220);


    //noFill();
    ellipse(100,100,80,20);
    fill(255);
    ellipse(100,100,40,10);

    line(60,100,60,170);
    line(140,100,140,170);

    line(140,100,190,110);
    line(140,170,190,180);
    line(190,110,190,180);

    bezier(60,170,80,180,120,180,140,170);
    pop(); //this whole push pop is a "drawing" of the toiletpaper roll


  }

  move() {

    this.ypos+=this.speed; //the downward movement of the toiletpaper roll


  }

}

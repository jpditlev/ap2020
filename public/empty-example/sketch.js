function setup() {
  createCanvas(1200, 800);
}

function draw() {
  if (mouseIsPressed) {
    fill(0);
  } else {
    fill(255);
  }
  ellipse(mouseX, mouseY, 70, 70);
}

let running;
let len = 0;
let speed = 2;


//the gif of the running stick figure is preloaded and createt.
//createImg() is used because image() only uses the first frame of a gif. gifs are DOM elements
function preload(){
  running = createImg("running.gif");
}




function setup(){
  createCanvas(windowWidth,windowHeight);

textSize(30);
textAlign(CENTER);

running.size(80, 80); //gif size reduced to 50x50


frameRate(5);
}



function draw(){

background(255);

if(frameCount > 150){
text(frameCount, windowWidth/2, windowHeight/5);
}

running.position(windowWidth/2-40, windowHeight/2); //gif is called and placed in x,y
loadingbar();  //referes to function loadingbar()
}



//function which shows the loadingbar is created here
function loadingbar(){
rectMode(CENTER)
fill(255);
rect(windowWidth/2,windowHeight/1.5,100,10)

len+=speed;


//the "if" statement is used to make the loading bar progress and retreat
//it is read as when len exceedes 90 (which is equal to 90% because the length
//of the white rectangle is 100) it starts to go towards 0 again, because the speed
//becomes negative speed. When the length reaches less or equal to 0, the speed becomes
//positive again and thus makes it grow once again
if (len > 90 || len <= 0){
  speed*=-1
}
rectMode(CORNER);
  fill(0);
  rect(windowWidth/2-50, windowHeight/1.5-5, len, 10);
}

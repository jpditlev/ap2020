![ScreenShot](miniEx03.png)

RUNME:https://jpditlev.gitlab.io/ap2020/miniEx03/



My throbber excercise is split in 3 parts. The first part is the running stick figure which is a gif I found and put into my program.
The second part is a loading bar which consists of a white rectangle on which a black rectangle progresses and retreats.
The third part is a framecounter which is first shown after 150 frames.

I chose to do this 3 split to make a point about time, time consumption and representation of time. 
The running stick figure derives from the concept of the throbber we know; the spinning wheel that appears when something isn't working while watching Netflix, YouTube, Twitch and so on.
That particular throbber is representing a constant progress towards finish. We don't know what technically is happening
but something is constantly working towards showing us the movie again. I find that perculiar because it might all just be an illusion.
Someone has chosen to show us this icon, that reprents progress, to let us know that something is constantly working towards an end product, but it tells us absolutely nothing.
Nothing could be happening behind the scenes, it could already be loaded but now shown etc. We wait for
the "thing" to be done preparing what should be shown without knowing if anything is happening, how far it is and if it ever will be done.


This is there the progress bar proves useful (or does it?). The loading bar shows
how far it is with loading whatever content you're waiting for and thus makes it possible to
somehow know how long it wil be: It took 2 seconds to get to 20 percent, I might aswell wait it out.
The funny thing is this also is an illusion. The machine or program has no concept of time
or far it is the in the progress but someone chose it should be possible to see how far it is.
It could with lightning speed go to 98% and then get stuck for 2 hours. The percentages
have no grounding in actual reality but are what someone chose to be representative of
what has been done and what is still missing. I included the loading bar which never reaches
100% even though the program is constantly working towards presenting whatever you are waiting for (the running stick figure).
It's a way to highlight the previous points: They are illusions and have no ground in actual time or progress.
The framecounter that appears after 150 frames is to really show the viewer how long they have been waiting for
absolutely nothing and there might never appear anything because the throbber and loading bar has no grounding in reality.
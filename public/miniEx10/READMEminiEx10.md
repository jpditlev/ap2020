![ScreenShot](miniex10.png);
**2 flowcharts of 2 different ideas for our final project. Made in collaboration with Mads Lindgaard, Sophia McCulloch & Mikkel Heinrich**


**My individual flowchart is of my miniEx04 and can be found in pdf format in this map, miniEx10**

I have made a flowchart over my own program from miniEx04, Capture All. The concept of the program is essentially really simple and easy to explain and grasp: Whenever you press a key on your keyboard, that specific key appears on screen and rains down. That is why the flowchart for that program focuses a lot on the technicalities of the program. I seek, through the use of the fllowchart, to communicate how the program technically works, but I'm communicating it to a  programmer or software engineer, I'm communicating what lies behind the program to the observer of the artwork. This also works well as en extension to the program, since I with the program sought to comment on the level of surveillance and data capture that is present at all times, watching and saving your every move. This flowchart could in this sense prove as an invitation to let people know what is behind the curtains in the various data capturing ways. However, at this point in time, the only people who would be able to understand what is going on behind the curtains are people with knowledge and experience with software and coding languages, just like my flowchart does.  It gives an outline and idea of the technicalities to a "regular" observer, but the person with experience on programming would get more out of it.



The flowcharts we made explain 2 different ideas.

The first program is a game where you are presented with 6 cards, half red and other half black. You choose a card, if it's read, you move on, if it's black, you lose. You are never able to win though. When you lose, the program takes control of your cursor on the screen.

The other program makes use of an ASCII library and your webcam. When the webcam is turned on, the displayed image becomes displayed using ASCII characters. Alas, you are turned into data symbols in its most literal sense. In extension, a .json file would be included, which would make all kinds of data rain down as it was being taken and downloaded by someone.

These two flowcharts differ from the other I made. These two flowcharts are created to communicate a concept, to give an outline of an idea. One of the flowcharts makes use of some simple programming terms, but that doesn't stray too far away from the otherwise simple, laymen-like language. The simple language is necessary for the flowcharts to prove useful the way they are meant to be useful. They are to communicate a concept, so everyone must be able to understand it. If this were a project where multiple parties are involved, then shouldn't only the programmers and software engineers be involved, but all parties involved should have the opportunity to know what is happening and be able to bring input.


I have some thoughts on my own flowchart, which are not as present in the two others. Since the first one tries to explain algorithmic procedures and the technicalities, it also very quickly became quite apparent to me how difficult it quickly proved to be. It might be a complex program for me, but it is very, very simple in the grand scheme of things. It will be almost impossible to do this type of flowchart on the much bigger program, when they are so intricate and complex. So many parts of the programs overlap in many different ways, so it may be more confusing than useful. This is where the second type of flowchart could be used; to explain the program in its entirety and not going too much into the specific details.

Regarding the two group flowcharts: Another aspect of these flowcharts is that they could make it easy for us to add more steps or options into the program and very quickly visualize the changes by just adding these to the flowcharts of the "base program", which we have already made.

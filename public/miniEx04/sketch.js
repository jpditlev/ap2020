let x1 = [];
let y1 = [];
let size = [];
let letter = [];

let gravity = 7;

function setup() {

  createCanvas(800, 800);
  for (let i = 0; i < 100; i++){
    x1.push(random(0, 800))
    y1.push(random(0, 800))
    size.push(random(14, 38)) //my arrays are here made so the x and y position
    //are randomized from 0-800 (size of canvas) and the size of the input text
    //is between 14 and 38. Since i has to be smaller than 100, there is only
    //room for 100 spots in the array. So only 100 letters can be fit into
    //the array. "array.push()" adds value to the chosen arrau.
  }
}

function draw() {

  background(182, 178, 178);

  for (let i = 0; i < x1.length; i++){ //the value in the array is chosen as long
    //as i is smaller than the length of the array (100 as previously stated)
    textSize(size[i])
    text(letter[i], x1[i], y1[i])
  }

  for (let i = 0; i < y1.length; i++){ //same as above but with y.
    if (y1[i] > 800){ //if the letter goes off the screen (because max y in this
    //canvas is 800), the arrays are reset.
      y1[i] = 0
      x1[i] = random(0, 800)
      size[i] = random(14, 38)
    }
    y1[i] += gravity;

  }
}


function keyPressed(){
  letter.push(key); //append adds a value to the end of an array.
}

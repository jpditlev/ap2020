let forestfloor;

function preload(){
  forestfloor = loadImage("forestfloor.jpeg");
}

function setup(){
createCanvas(1200,700)
  forestfloor.resize(1170,700);
  image(forestfloor,0,0);

}

function draw(){
translate(150,120); //using translate to move the first snail (like it was an object);

//1st snail
fill(81,204,0);
ellipse(170,210,20,200); //the two antennas
ellipse(230,210,20,200);

fill(255);
ellipse(170,120,40); //the outline of the eyes
ellipse(230,120,40);

fill(0);
ellipse(165,115,20); //the pupils
ellipse(235,115,20);

fill(81,204,0);
ellipse(200,300,130,80); //the body/face

//using text and translate + rotate to create mouth
fill(0);
textSize(70);

push();
translate(470,80);
angleMode(DEGREES);
rotate(90);
text("D",200,300);
pop();


translate(300,0); //to move the second snail
//2nd snail
fill(81,204,0);
ellipse(370,210,20,200);
ellipse(430,210,20,200);

fill(255);
ellipse(370,120,40);
ellipse(430,120,40);

fill(0);
ellipse(365,115,20);
ellipse(435,115,20);

fill(81,204,0);
ellipse(400,300,130,80);

fill(0);
textSize(70);

push();
translate(120,525);
angleMode(DEGREES);
rotate(-90);
text("D",200,300);
pop();
}

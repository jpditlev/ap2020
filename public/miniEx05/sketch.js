let emoji;
let x1 = 0

let happybutton;
let sadbutton;
let indifferentbutton;
let disapprovingbutton;
let surprisebutton;
let erasebutton;


function setup(){

  createCanvas(1280,720);

  background(122,255,255);

  noStroke();
  fill(255,179,255);
  rect(0,0,300,720);

  happybutton = createButton("happy");
  sadbutton = createButton("sad");
  indifferentbutton = createButton("indifferent");
  disapprovingbutton = createButton("disapproving");
  surprisebutton = createButton("surprise me");
  erasebutton = createButton("eraser");


  happybutton.size(100,100);
  happybutton.position(25,25);
  happybutton.mousePressed(drawHappy);
  happybutton.style("background-color","white");
  happybutton.style("color","#e39fe1");
  happybutton.style("border-color","#FFB3FF");

  sadbutton.size(100,100);
  sadbutton.position(175,25);
  sadbutton.mousePressed(drawSad);
  sadbutton.style("background-color","white");
  sadbutton.style("color","#e39fe1");
  sadbutton.style("border-color","#FFB3FF");

  indifferentbutton.size(100,100);
  indifferentbutton.position(25,175);
  indifferentbutton.mousePressed(drawIndifferent);
  indifferentbutton.style("background-color","white");
  indifferentbutton.style("color","#e39fe1");
  indifferentbutton.style("border-color","#FFB3FF");

  disapprovingbutton.size(100,100);
  disapprovingbutton.position(175,175);
  disapprovingbutton.mousePressed(drawDisapproving);
  disapprovingbutton.style("background-color","white");
  disapprovingbutton.style("color","#e39fe1");
  disapprovingbutton.style("border-color","#FFB3FF");

  surprisebutton.size(100,100);
  surprisebutton.position(25,325);
  surprisebutton.mousePressed(drawSurprise);
  surprisebutton.style("background-color","white");
  surprisebutton.style("color","#e39fe1");
  surprisebutton.style("border-color","#FFB3FF");

  erasebutton.size(100,100);
  erasebutton.position(175,325);
  erasebutton.mousePressed(eraser);
  erasebutton.style("background-color","#616161");
  erasebutton.style("color","white");
  erasebutton.style("border-color","#FFB3FF")
}


function draw(){

  if (mouseX > 300 && x1 == 0 && mouseIsPressed){
    noCursor();
    fill(255,179,255),
    text(emoji, mouseX, mouseY);
  } else {
    cursor();
  }

  if (mouseX > 324 && x1 == 1 && mouseIsPressed){
    cursor(CROSS);
    fill(122,255,255);
    ellipse(mouseX,mouseY,50,50);
  }

}


function drawHappy(){

  x1 = 0;
  emoji = ":(";
}

function drawSad(){

  x1 = 0;
  emoji = ":)";
}


function drawIndifferent(){

  x1 = 0;
  emoji = ":|";
}


function drawDisapproving(){

  x1 = 0;
  emoji = "⊙︿⊙";
}


function drawSurprise(){

  x1 = 0;
  emoji = "." //design your own emoji here ;)
}


function eraser(){

  x1 = 1;
}

let fr=5
var song;


function preload() {
  song = loadSound("pinkfloyd.mp3");
}

function setup(){
createCanvas(600,600)
background(0);

song.play();

}

function draw(){

strokeWeight(3)
fill(0,0,0)
triangle(300,90,100,420,500,420)
stroke(255,255,255)

line(0,280,200,255)

strokeWeight(0)
fill(230,230,230)
triangle(200,255,390,240,428,300)

frameRate(fr)
strokeWeight(0)
fill(255,random(255),random(255))
quad(390, 240, 600, 265, 600, 285, 398, 250)
fill(255,random(255),random(255))
quad(398,250,600,285,600,305,403,260)
fill(random(255),255,random(255))
quad(403,260,600,305,600,325,408,270)
fill(random(255),255,random(255))
quad(410,270,600,325,600,345,415,280)
fill(random(255),random(255),255)
quad(415,280,600,345,600,365,422,290)
fill(random(255),random(255),255)
quad(422,290,600,365,600,385,428,300)
}

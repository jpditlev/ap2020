![ScreenShot](miniEx01.png)


**Husk at sætte lyd til før RUNME køres**
RUNME: https://jpditlev.gitlab.io/ap2020/miniEx01/

This program is an animated take on Pink Floyd's "Dark Side of the Moon" while "The Great Gig in the Sky" from the aforementioned album plays.

I have no prior coding experience so this is my absolute first impressions. It has been very overwhelming and I felt lost already from the start but still excited and curious. First time I sat down with it alone I spent 2 hours just playing around with different functions and syntaxes to see if I could make anything of it. It's quite easy to modify the different functions but as I tried to combine it started to get complicated. Several times my program went from working flawlessly to me being presented with a white browser. The webconsole made me realize I needed to put an extra bracket somewhere. The most difficult part so far is tuning my way of thinking to being so literal and slavish and very detail oriented. You have to be really thorough but that's also what makes it fun to experiment with and what results in 3 hours suddenly passing because I just played around with different functions. And it feel extremely good when you run the program and it just works. A big little "YES" moment

A short summary: It has been overwhelming, intimidating, fun, difficult, pleasurable



Learning to write code, and in this case JavaScript, is sort of like learning af new language. If I say something in English where the structure of the sentence is totally randomized, my sentence would make little sense, but you may be able to understand it despite of it. When dealing with attempting to communicate with the computer, by writing code in p5.js, the principles are the same, but there are some differences too. The rule of language and sentence strcuture applies to both kinds of languages, so I have to "talk" (write my code) according to the rules of the language for the machine to understand me and what I want. The difference is that people can make sense of non-sensible things because of experience. The machine cannot. The machine goes from the top of the script to the bottom and takes everything literally, so that is why I was being frustrated; a little difference, like changing a comma or a single bracket will make it or break it.


This introduction has shown me the ups and downs of coding, and the literacy perspective on programming and coding has helped me understand how the "langauge" of writing and reading code works. That doesn't mean I'm a perfect reader og writer yet, but it definitely helps as an approach to learning to read and write code.

